package com.stations.test.objects;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class Tap {

	Long unixTimestamp;
	Integer customerId;
	String station;

	public Long getUnixTimestamp() {
		return unixTimestamp;
	}

	public void setUnixTimestamp(Long unixTimestamp) {
		this.unixTimestamp = unixTimestamp;
	}

	public DateTime getDateTimeSeconds() {
		if (unixTimestamp != null) {
			return new DateTime(unixTimestamp * 1000, DateTimeZone.UTC);
		}
		return null;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	@Override
	public String toString() {
		return "Tap [customerId=" + customerId + ", station=" + station + "/ "+ getDateTimeSeconds() + "]";
	}

}
