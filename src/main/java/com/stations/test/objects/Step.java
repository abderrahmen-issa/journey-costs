package com.stations.test.objects;

public class Step {

	private String station;
	private Long unixTimestamp;

	public Step(String station, Long unixTimestamp) {
		this.station = station;
		this.unixTimestamp = unixTimestamp;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public Long getUnixTimestamp() {
		return unixTimestamp;
	}

	public void setUnixTimestamp(Long unixTimestamp) {
		this.unixTimestamp = unixTimestamp;
	}

	@Override
	public String toString() {
		return "Step [station=" + station + ", at=" + unixTimestamp + "]";
	}

}
