package com.stations.test.objects;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TapsInput {

	@JsonProperty("taps")
	private Collection<Tap> taps;

	public Collection<Tap> getTaps() {
		return taps;
	}

	public void setTaps(Collection<Tap> taps) {
		this.taps = taps;
	}
}
