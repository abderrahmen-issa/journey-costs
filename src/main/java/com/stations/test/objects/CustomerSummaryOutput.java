package com.stations.test.objects;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomerSummaryOutput {

	@JsonProperty("customerSummaries")
	private Collection<CustomerSummary> customerSummaries;

	public CustomerSummaryOutput(Collection<CustomerSummary> customerSummaries) {
		this.customerSummaries = customerSummaries;
	}

	public Collection<CustomerSummary> getCustomerSummaries() {
		return customerSummaries;
	}

	public void setCustomerSummaries(Collection<CustomerSummary> customerSummaries) {
		this.customerSummaries = customerSummaries;
	}

}
