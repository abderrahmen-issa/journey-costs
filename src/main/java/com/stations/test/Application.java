package com.stations.test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.stations.test.objects.CustomerSummary;
import com.stations.test.objects.CustomerSummaryOutput;
import com.stations.test.objects.Step;
import com.stations.test.objects.Tap;
import com.stations.test.objects.TapsInput;
import com.stations.test.objects.Trip;

/**
 * 
 * @author Abderrahmen ISSA
 *
 */
@SpringBootApplication
public class Application {
	
	private static Map<Integer, List<String>> plan = new LinkedHashMap<>();
	private static Map<Integer, List<Integer>> costs = new LinkedHashMap<>();

	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		System.out.println("\n---------main-----------------");
		System.out.println(args.length);
	    System.out.println(Arrays.toString(args));
	    System.out.println("--------------------------");	   
		
		// initialize data
		initStations();
		initCosts();

		// Reading json file
		TapsInput readValues = new ObjectMapper().readValue(new File(args[0]),
				new TypeReference<TapsInput>() {
				});

		Map<Integer, List<Trip>> summary = groupByCustomerId(readValues.getTaps());
		List<CustomerSummary> customerSummaries = getCustomerSummaries(summary);

		// Writing to a file
		new ObjectMapper().writeValue(new File(args[1]), new CustomerSummaryOutput(customerSummaries));

	}

	/**
	 * 
	 * @param summary
	 * @return
	 */
	private static List<CustomerSummary> getCustomerSummaries(Map<Integer, List<Trip>> summary) {
		List<CustomerSummary> result = new ArrayList<>();
		for (Integer customerId : summary.keySet()) {
			List<Trip> trips = summary.get(customerId);
			CustomerSummary customerSummary = new CustomerSummary();
			customerSummary.setCustomerId(customerId);
			customerSummary.setTotalCostInCents(calculateTotalCost(trips));
			customerSummary.setTrips(trips);
			result.add(customerSummary);
		}

		return result;
	}

	/**
	 * 
	 * @param trips
	 * @return
	 */
	private static Integer calculateTotalCost(List<Trip> trips) {
		Integer result = 0;
		for (Trip t : trips) {
			result += t.getCostInCents();
		}
		return result;
	}

	/**
	 * 
	 * @param taps
	 * @return
	 */
	private static Map<Integer, List<Trip>> groupByCustomerId(Collection<Tap> taps) {
		// Group journey steps by customer
		Map<Integer, List<Step>> tmp = new HashMap<>();
		for (Tap tap : taps) {
			Integer customerId = tap.getCustomerId();
			Step step = new Step(tap.getStation(), tap.getUnixTimestamp());
			if (tmp.containsKey(customerId)) {
				tmp.get(customerId).add(step);
			} else {
				tmp.put(customerId, new ArrayList<Step>(Arrays.asList(step)));
			}
		}

		// foreach customer calculate trip and cost
		Map<Integer, List<Trip>> result = new HashMap<>();
		for (Integer customerId : tmp.keySet()) {
			List<Step> steps = tmp.get(customerId);
			List<Trip> calculatedTrips = calculateTrips(steps);
			result.put(customerId, calculatedTrips);
		}

		return result;
	}

	/**
	 * 
	 * @param steps
	 * @return
	 */
	private static List<Trip> calculateTrips(List<Step> steps) {
		List<Trip> result = new ArrayList<>();
		int i = 0;
		while (i < steps.size()) {
			String stationFrom = steps.get(i).getStation();
			String stationTo = steps.get(i + 1).getStation();
			Trip trip = new Trip();
			trip.setStationStart(stationFrom);
			trip.setStationEnd(stationTo);
			trip.setZoneFrom(getZone(stationFrom));
			trip.setZoneTo(getZone(stationTo));
			trip.setStartedJourneyAt(steps.get(i).getUnixTimestamp());
			trip.setCostInCents(getCost(trip.getZoneFrom(), trip.getZoneTo()));
			result.add(trip);
			i += 2;
		}
		return result;
	}

	/**
	 * 
	 * @param zoneFrom
	 * @param zoneTo
	 * @return
	 */
	private static Integer getCost(Integer zoneFrom, Integer zoneTo) {
		List<Integer> list = costs.get(zoneFrom);
		return list.get(zoneTo - 1);
	}

	/**
	 * 
	 * @param station
	 * @return
	 */
	private static Integer getZone(String station) {
		for (Integer key : plan.keySet()) {
			List<String> value = plan.get(key);
			if (value.contains(station)) {
				return key;
			}
		}
		return null;
	}

	/**
	 * 
	 */
	private static void initStations() {
		plan.put(1, Arrays.asList("A", "B"));
		plan.put(2, Arrays.asList("C", "D", "E"));
		plan.put(3, Arrays.asList("C", "E", "F"));
		plan.put(4, Arrays.asList("F", "G", "H", "I"));
	}

	/**
	 * 
	 */
	private static void initCosts() {
		// ------(from, ----------( 1 , 2 , 3 , 4 )
		costs.put(1, Arrays.asList(240, 240, 280, 300));
		costs.put(2, Arrays.asList(240, 240, 280, 300));
		costs.put(3, Arrays.asList(280, 280, 200, 200));
		costs.put(4, Arrays.asList(300, 300, 200, 200));
	}

}
